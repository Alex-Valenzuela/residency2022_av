# Residency 2022

## Project: Identification of potentials in electroencephalic signals, evoked through virtual experiences of movement.

### Description
This project consists of the creation of an automatic method for the detection of evoked potentials in electroencephalic signals through existing signal banks.

### Index

#### Preprocessing:
1. [Reformat of data](notebooks/RawDataReformating.ipynb)
2. [Assembled average](notebooks/Preprocessing.ipynb)
3. [Segmentation of time series](notebooks/TimeSegmentation.ipynb)

#### Processing:
- [Feature extraction](notebooks/Processing.ipynb)

#### Data analysis of features:
- [Feature Analysis](notebooks/FeatureAnalysis.ipynb)

#### Classification:
1. [Classifier devolpment](notebooks/Classifier.ipynb)
2. [Classifier ROC curves](notebooks/ClassifierCurves.ipynb)

### Reproduction deployment
To run the Jupyter Notebooks in your web browser, click on the launch link below. It may take a couple of minutes to launch Jupyter Lab.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Alex-Valenzuela%2Fresidency2022_av/main)

### Acknowledgements
M.S. Arturo Sotelo for his advice, mentorship and support in this project.

### Citation
Kevin A. Valenzuela, Identification of potentials in electroencephalic signals, evoked through virtual experiences of movement. Available online: https://gitlab.com/Alex-Valenzuela/residency2022_av (2022).

### License
This projects licesend by a BSD 3-Clause License.

### Project status
Based on the results of the project, the new version of it will be resume in another project using a different process for the processing and classification of the time classes.

