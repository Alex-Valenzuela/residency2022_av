# Datasets

Images related to the adquisition methodology of electroencephalic signals.

| Article | File Name | Download Page | Download Link | Date Downloaded |
| :---    | :---      | :---          | :--           | :--             |
| Random forests in non-invasive sensorimotor rhythm brain-computer interfaces: a practical and convenient non-linear classifier | Laplacian_electrode_placement_scheme.png | http://bnci-horizon-2020.eu/database/data-sets | https://www.degruyter.com/document/doi/10.1515/bmt-2014-0117/asset/graphic/j_bmt-2014-0117_fig_002.jpg | 2022-07-09 |
| Random forests in non-invasive sensorimotor rhythm brain-computer interfaces: a practical and convenient non-linear classifier | Evaluation_experiment_run_sequence.png | http://bnci-horizon-2020.eu/database/data-sets | https://www.degruyter.com/document/doi/10.1515/bmt-2014-0117/asset/graphic/j_bmt-2014-0117_fig_003.jpg | 2022-07-09 |
| Identification of potentials in electroencephalic signals, evoked through virtual experiences of movement | Evaluation_experiment_run_sequence.png | https://gitlab.com/Alex-Valenzuela/residency2022_av |  | 2022-07-10 |